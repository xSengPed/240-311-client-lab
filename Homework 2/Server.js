var net = require('net')
var HOST = '127.0.0.1'
var PORT = 5555

let number = 10
let i = 0
net.createServer(function(sock){
    // console.log('CONNECTED: ' + sock.remoteAddress +' : '+ sock.remotePort);

    sock.on('data',function(data){
        
        if(i === 5){
            sock.write('END')
            console.log('Terminated')
            i = 0
        }

        if(data && data.toString().length === 10) {
            console.log('Client DATA ID : '+data)
            console.log('PASS')
            sock.write('OK')
        }
        else if(data.toString() === number.toString())
        {   
            console.log('Client DATA : '+data)
            console.log('STATUS : BINGO')
            sock.write('BINGO')
        }
        else if(data.toString !== number.toString())
        {
            console.log('Client DATA : '+data)
            sock.write('WRONG')
            
        }
        i++
        

    });
    sock.on('close',function(data){
         console.log('CLOSED: ');
    });
    sock.on('error',()=>{
        console.log('Error Detected')
    })
}).listen(PORT,HOST);

console.log('Server listening on ' + HOST +' : '+ PORT); 